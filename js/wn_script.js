

jQuery(document).ready(function(){ 

	jQuery('.add-to-cart').on('click', function () {
        var cart = jQuery('.shopping-cart');

        var imgtodrag = jQuery(".wn_caption").find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                	'opacity': '0.5',
                    'position': 'absolute',
                    'z-index': '100'
            })
                .appendTo(jQuery('body'))
                .animate({
                	'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75
            }, 1000);
            

            imgclone.animate({
                	'width': 0,
                    'height': 0
            }, function () {
                jQuery(this).detach()
            });
        }
    });

    jQuery('.add-to-cart2').on('click', function () {
        var cart2 = jQuery('.shopping-cart2')

        var imgtodrag2 = jQuery(this).closest(".wn_product_case").find(".product_case_img img"); 
        if (imgtodrag2) {
            var imgclone = imgtodrag2.clone()
                .offset({
                top: imgtodrag2.offset().top,
                left: imgtodrag2.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'z-index': '100'
            })
                .appendTo(jQuery('body'))
                .animate({
                'top': cart2.offset().top + 10,
                    'left': cart2.offset().left + 10,
                    'width': 75
            }, 1000);
            

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                jQuery(this).detach()
            });
        }
    });
 //-----------------------
    jQuery(".wn_filter_more").on("click", function(){
          jQuery(this).toggleClass('active');
          jQuery(this).prev().slideToggle();

       })
//-----------------------
    jQuery(".wn_filter_more_lk").on("click", function(){
         jQuery(this).parents().next().slideToggle();

       })

 //-----responsive tabs----

    if(jQuery(".wn_tabs").length){
        // jQuery(".wn_tabs").easyResponsiveTabs(); 

        jQuery('.wn_tabs_parents').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'wn_tabs_parent' // The tab groups identifier
        });
        
        jQuery('.wn_tabs_child').easyResponsiveTabs({
            type: 'default',
            width: 'auto',
            fit: true,
            closed: 'accordion',
            tabidentify: 'wn_tabs_children'
        });


    };


    
//------------zoom-------------------------
    if(jQuery(".containerZoom").length){

 
        jQuery(".containerZoom").imagezoomsl({ 
            
            zoomrange: [1, 12],
            innerzoom: true,
            magnifierborder: "none"   
        });
   

        jQuery(".tmb_caption img").click(function(){

            var that = this,
                z_adress=jQuery(this).attr("data-tmb-large");
           
            jQuery(".containerZoom").fadeOut(600, function(){

                jQuery(this).attr("src", z_adress)
                       .attr("data-large", z_adress)
                       .fadeIn(1000);

                     jQuery(this).parents('.wn_product_caption').next('.fancybox').attr('href', z_adress);   
            });

            return false;


        }); 
    }
  


});



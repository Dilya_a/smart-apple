jQuery('#slider').nivoSlider({
	pauseTime: 5000,
	afterLoad: function(){ jQuery('#slider-loader').fadeOut(500, function(){jQuery('#slider').fadeIn(500);}); }
});
jQuery("#slider-2").nivoSlider({
	effect:'sliceDownLeft,sliceDownRight',
	slices:15,
	boxCols:8,
	boxRows:4,
	animSpeed:500,
	pauseTime:6500,
	startSlide:0,
	directionNav:false,
	controlNav:true,
	controlNavThumbs:false,
	pauseOnHover:false,
	manualAdvance:false,
	afterLoad: function(){ jQuery('#slider-2').css('min-height',''); }
});
